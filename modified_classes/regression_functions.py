from iapws import _Sublimation_Pressure



def T_from_h_ice(h):
    """
    

    Parameters
    ----------
    h : float (in kJ/kg)
        Specific enthalpy of the ice phase(supposed to be independant of the pressure)

    Returns
    -------
    T : float (in K)
        Corresponding temperature 

    """
    
    
    
    T = -891 - 11.58*h - 0.04088*h**2 - 6.151e-5*h**3 - 3.503e-8*h**4
    if T < 50 or T > 273.15:
        print(f"Warning: T is outside of the range [50, 273.15]K (T_from_ice(h) function),T=",T)
        
    if T<0:
        return 0
    
    return T

def T_from_h_steam(h):
    """
    

    Parameters
    ----------
    h : float (in kJ/kg)
        Specific enthalpy of the steam phase(supposed to be independant of the pressure)

    Returns
    -------
    T : float (in K)
        Corresponding temperature 

    """
    
    
    T = -4951 + 7.248*h - 0.004356*h**2 + 1.257e-6*h**3 - 1.36e-10*h**4
    if T < 50 or T > 273.15:
        print(f"Warning: T is outside of the range [50, 273.15]K (T_from_steam(h) function),T=",T)
    if T<0:
        return 0
    
    return T


def Sublimation_temperature(p):
    """
    

    Parameters
    ----------
    p : float (in MPa)
        Pressure in MPa.

    Returns
    -------
    T : float (in K)
        Corresponding temperature on the sublimation curve.

    """
    
    temperature_min = 0
    temperature_max = 273    
    precision = 0.01  
    while temperature_max - temperature_min > precision:
        temperature = (temperature_min + temperature_max) / 2  
        p_estimee = _Sublimation_Pressure(temperature)  
        if p_estimee < p:
            temperature_min = temperature  
        else:
            temperature_max = temperature  
    return (temperature_min + temperature_max) / 2 


def h_steam(T):
    h= -9.21941576e-08*T**3 + 7.37936434e-05*T**2 + 1.83397067e+00*T + 1.99680306e+03 
    return h    


def h_steam_linear(T):
    a=1.8499187516938336
    b=1995.8886858138708
    return a*T+b


def T_steam_linear(h):
    a=.5405637476457987
    b=-1078.9049082941892
    return a*h + b


def T_ice_linear(h):
    a=.7471884035218935
    b=540.7899387830403
    return a*h + b


    
    