from modified_classes.Ideal_gas import Ideal_gas
from iapws import _Ice, IAPWS95
from modified_classes.regression_functions import Sublimation_temperature,T_from_h_ice,T_from_h_steam

from modified_classes.regression_functions import T_ice_linear, T_steam_linear 

class Water_properties:
    '''
    Class responsible for collecting all needed properties depending of the phase using several IAPWS module
    '''
    def __init__(self, state):
        self.P = state[0] * 0.1 #bar => mPa
        self.h = state[1] / 18.015 #kJ/kmol => kJ/kg
        self.T_sub = Sublimation_temperature(self.P)      
        self.sublimating_ice = _Ice(T=self.T_sub, P=self.P) #creating sublimating ice and saturated steam object
        self.saturated_steam = Ideal_gas(T=self.T_sub, P=self.P) 
        self.h_sub = self.sublimating_ice["h"] #determination of the twophase zone
        self.h_sat = self.saturated_steam.h
        self.steam_saturation = self._determine_steam_saturation()
        self.T = self._determine_T()
        
        self.vapor_quality= self._determine_vapor_quality()

    def _determine_vapor_quality(self):
        if self.h <= self.h_sub:
            return 0
        elif self.h >= self.h_sat:
            return 1
        else:
            return (self.h - self.h_sub) / (self.h_sat - self.h_sub)

        


    def _determine_steam_saturation(self):
        if self.h <= self.h_sub:
            return 0
        elif self.h >= self.h_sat:
            return 1
        else:
            return (self.h - self.h_sub)* self.sublimating_ice_density / ( (self.h -self.h_sub)*self.sublimating_ice_density + (self.h_sat - self.h)*self.saturated_steam_density)

    def _determine_T(self):
        if self.steam_saturation == 0:
            return T_ice_linear(self.h)
        elif self.steam_saturation == 1:
            return T_steam_linear(self.h)
        else:
            return self.T_sub    

    @property
    def steam_viscosity(self):
        return IAPWS95(T=self.T, P=self.P).mu

    @property
    def sublimating_ice_density(self): # ice density on the sublimating curve 
        return self.sublimating_ice["rho"]

    @property
    def saturated_steam_density(self): # steam density on the sublimating curve
        return self.saturated_steam.rho
    
    @property
    def supercooled_ice_density(self): # ice density in the single phase ice area
        return _Ice(self.T,self.P)['rho']
    
    @property
    def overheated_steam_density(self): # steam density in the single phase steam area
        return Ideal_gas(self.T,self.P).rho
    
    @property
    def TP_density(self): #two-phase density using vapor quality method
        if self.vapor_quality == 0:
            return self.supercooled_ice_density
        elif self.vapor_quality ==1:
            return self.overheated_steam_density
        else:
            return (1-self.vapor_quality) * self.sublimating_ice_density + (self.vapor_quality) * self.saturated_steam_density
    
    
    
    
        
        
    
    
        
