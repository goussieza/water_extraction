from darts.engines import property_evaluator_iface
from modified_classes.Water_properties import Water_properties
from iapws import _Sublimation_Pressure, _Ice
from modified_classes.Ideal_gas import Ideal_gas

'''
Custom evaluators calling 
'''

class custom_temperature_evaluator(property_evaluator_iface):
    def __init__(self):
        super().__init__()
    def evaluate(self, state):
        return Water_properties(state).T

class custom_ice_enthalpy_evaluator(property_evaluator_iface):
    def __init__(self):
        super().__init__()
    def evaluate(self, state):
        if Water_properties(state).steam_saturation == 0:
            return Water_properties(state).h * 18.015 #kJ/kmol
        if Water_properties(state).steam_saturation == 1:
            return 0.0
        else:
            return Water_properties(state).h_sub * 18.015        
        
class custom_steam_enthalpy_evaluator(property_evaluator_iface):
    def __init__(self):
        super().__init__()
    def evaluate(self, state):
        # print(Water_properties(state).P)
        # print(Water_properties(state).T)
        # print('custom_steam_enthalpy_evaluator evaluate called')
        # print('steam saturation:',Water_properties(state).steam_saturation)
        
        if Water_properties(state).steam_saturation == 0:
            return 0.0
        if Water_properties(state).steam_saturation == 1:
            return Water_properties(state).h * 18.015
        else:
            return Water_properties(state).h_sat * 18.015

class custom_ice_saturation_evaluator(property_evaluator_iface):
    def __init__(self):
        super().__init__()
    def evaluate(self, state):
        return 1 - Water_properties(state).steam_saturation

class custom_steam_saturation_evaluator(property_evaluator_iface):
    def __init__(self):
        super().__init__()
    def evaluate(self, state):
        # print(state)
        # print('h_sat',Water_properties(state).h_sat)
        # print('h_sub',Water_properties(state).h_sub)
        # print('h',Water_properties(state).h)        
        return Water_properties(state).steam_saturation

class custom_ice_relperm_evaluator(property_evaluator_iface):
    def __init__(self):
        super().__init__()
    def evaluate(self, state):
        return 0.0

class custom_steam_relperm_evaluator(property_evaluator_iface):
    def __init__(self):
        super().__init__()
    def evaluate(self, state):
        return Water_properties(state).steam_saturation

class custom_ice_density_evaluator(property_evaluator_iface):
    def __init__(self):
        super().__init__()
    def evaluate(self, state):
        if Water_properties(state).steam_saturation == 0:
            return Water_properties(state).supercooled_ice_density / 18.015
        if Water_properties(state).steam_saturation == 1:
            return 0.0
        else:
            return Water_properties(state).sublimating_ice_density / 18.015

class custom_steam_density_evaluator(property_evaluator_iface):
    def __init__(self):
        super().__init__()
    def evaluate(self, state):
        if Water_properties(state).steam_saturation == 0:
            return 0.0
        if Water_properties(state).steam_saturation == 1:
            return Water_properties(state).overheated_steam_density /18.015
        else:
            return Water_properties(state).saturated_steam_density /18.015
        # else:
        #     return 0.015 #TEST PRESSURE PROPAGATION


class custom_ice_viscosity_evaluator(property_evaluator_iface):
    def __init__(self):
        super().__init__()
    def evaluate(self, state):
        return 0.0

class custom_steam_viscosity_evaluator(property_evaluator_iface):
    def __init__(self):
        super().__init__()
    def evaluate(self, state):
        return Water_properties(state).steam_viscosity *1000



class custom_total_enthalpy_evalutor(property_evaluator_iface):   #works only for single phase initial condition 
    def __init__(self, temp):
        super().__init__()
        self.T = temp
    def evaluate(self, state):
        P = state[0]*0.1
        T_sub =_Sublimation_Pressure(self.T)
        if self.T < T_sub:
            h =_Ice(T=self.T,P=P)['h']
        else:
            h = Ideal_gas(self.T, P=P).h
        print('total enthalpy etor called, h(kJ/kg)=',h)
        return h*18.015


class custom_rock_energy_evaluator(property_evaluator_iface):
    def __init__(self, rock):
        super().__init__()
        self.rock_table = rock

    def evaluate(self, state):
        # sat_steam_enthalpy = saturated_steam_enthalpy_evaluator()
        # sat_water_enthalpy = saturated_water_enthalpy_evaluator()
        # T = temperature_evaluator(sat_water_enthalpy, sat_steam_enthalpy)
        # # T = iapws_temperature_evaluator()
        # temperature = T.evaluate(state)

        temp = custom_temperature_evaluator()
        temperature = temp.evaluate(state)

        temperature_ref = self.rock_table[0][2]
        heat_constant = 1
        return (heat_constant * (temperature - temperature_ref))


class custom_rock_compaction_evaluator(property_evaluator_iface):
    def __init__(self, rock):
        super().__init__()
        self.rock_table = rock

    def evaluate(self, state):
        pressure = state[0]
        pressure_ref = self.rock_table[0][0]
        compressibility = self.rock_table[0][1]
        return (1.0 + compressibility * (pressure - pressure_ref))

    

    
    