
# -*- coding: utf-8 -*-
"""
Created on Wed May 31 14:50:48 2023

@author: Antoine.Goussiez
"""

from darts.engines import *
from darts.models.physics.iapws.iapws_property import *
from darts.models.physics.iapws.custom_rock_property import *

from modified_classes.custom_evaluators import *

      

class property_data():
    '''
    Class responsible for collecting all needed properties in geothermal simulation
    '''

    def __init__(self):
        self.temperature = custom_temperature_evaluator()       #Creating object of each property
        self.ice_enthalpy = custom_ice_enthalpy_evaluator() 
        self.steam_enthalpy = custom_steam_enthalpy_evaluator()  
        self.ice_saturation = custom_ice_saturation_evaluator()
        self.steam_saturation = custom_steam_saturation_evaluator() 
        self.ice_relperm = custom_ice_relperm_evaluator()  
        self.steam_relperm = custom_steam_relperm_evaluator()  
        self.ice_density = custom_ice_density_evaluator()  
        self.steam_density = custom_steam_density_evaluator()
        self.ice_viscosity = custom_ice_viscosity_evaluator()  
        self.steam_viscosity = custom_steam_viscosity_evaluator()  

        self.rock = [value_vector([3e-10, 1e-6, 100])]   #[pressure ref, compressibility, temperature ref]
        self.rock_compaction = custom_rock_compaction_evaluator(self.rock) 
        self.rock_energy = custom_rock_energy_evaluator(self.rock)  
        
        
               
       
class acc_flux_custom_evaluator_python(operator_set_evaluator_iface):
    def __init__(self, property_data):
        super().__init__()

        self.temperature       = property_data.temperature
        self.ice_enthalpy      = property_data.ice_enthalpy
        self.steam_enthalpy    = property_data.steam_enthalpy
        self.ice_saturation    = property_data.ice_saturation
        self.steam_saturation  = property_data.steam_saturation
        self.ice_relperm       = property_data.ice_relperm
        self.steam_relperm     = property_data.steam_relperm
        self.ice_density       = property_data.ice_density
        self.steam_density     = property_data.steam_density
        self.ice_viscosity     = property_data.ice_viscosity
        self.steam_viscosity   = property_data.steam_viscosity
        self.rock_compaction   = property_data.rock_compaction
        self.rock_energy       = property_data.rock_energy

    def evaluate(self, state, values):

        ice_enth    = self.ice_enthalpy.evaluate(state)
        steam_enth  = self.steam_enthalpy.evaluate(state)
        ice_den     = self.ice_density.evaluate(state)
        steam_den   = self.steam_density.evaluate(state)
        ice_sat     = self.ice_saturation.evaluate(state)
        steam_sat   = self.steam_saturation.evaluate(state)
        temp        = self.temperature.evaluate(state)
        ice_rp      = self.ice_relperm.evaluate(state)
        steam_rp    = self.steam_relperm.evaluate(state)
        ice_vis     = self.ice_viscosity.evaluate(state)
        steam_vis   = abs(self.steam_viscosity.evaluate(state)) #abs added to avoid negativ value at some T
        pore_volume_factor = self.rock_compaction.evaluate(state)
        rock_int_energy    = self.rock_energy.evaluate(state)
        pressure = state[0]

        # mass accumulation 
        values[0] = pore_volume_factor * (ice_den * ice_sat + steam_den * steam_sat) #Alpha
        # mass flux 
        
        if ice_vis==0: 
            values[1] = steam_den * steam_rp / steam_vis #Beta
        else:
            values[1] =ice_den * ice_rp / ice_vis + steam_den * steam_rp / steam_vis
        # fluid internal energy = water_enthalpy + steam_enthalpy - work
        # (in the following expression, 100 denotes the conversion factor from bars to kJ/m3)
        
        #alpha energy
        values[2] = pore_volume_factor * (ice_den * ice_sat * abs(ice_enth) + steam_den * steam_sat * steam_enth - 100 * pressure) #ice enth << 0 so alpha_e extra low
        # values[2] = pore_volume_factor * ( steam_den * steam_sat * steam_enth - 100 * pressure)

        # rock internal energy
        values[3] = rock_int_energy / pore_volume_factor
       
        # energy flux 
        
        if ice_vis==0: 
            values[4] = steam_enth * steam_den * steam_rp / steam_vis

        else:   
            values[4] = (ice_enth * ice_den * ice_rp / ice_vis + steam_enth * steam_den * steam_rp / steam_vis)
        # fluid conduction
        values[5] = 0
        # rock conduction
        values[6] = 1 / pore_volume_factor
        # temperature
        values[7] = temp
    
        print()        
        print("Alpha_m: %s Beta_m: %s Alpha_e: %s Beta_e: %s Temperature: %s" % (values[0], values[1], values[2], values[4], values[7]))
        print('state vector:', state)
        # print('values',values)
        # print('h_steam:',steam_enth)
        print('steam_den',steam_den)
        # print('steam_rp:',steam_rp)
        # print('steam visc:',steam_vis)
        print('steam enth etor kJ/kmol',steam_enth)
        # print('steam enth state vector kJ/kmol', state[1])
        # print('steam enth state kJ/kg',state[1]/18.015)
        print('Steam_sat:',steam_sat)
        print(steam_den * steam_sat * steam_enth)
        # print( pore_volume_factor * (ice_den * ice_sat * ice_enth + steam_den * steam_sat * steam_enth - 100 * pressure))
        # print('energy acc',value[2])
        print('pore volume factor',pore_volume_factor)
        print('pressure in accflux_etor',pressure*100)
        
        return 0
    
    
    
    
    
class geothermal_rate_custom_evaluator_python(operator_set_evaluator_iface):
    def __init__(self, property_data):
        super().__init__()

        self.temperature = property_data.temperature
        self.ice_enthalpy = property_data.ice_enthalpy
        self.steam_enthalpy = property_data.steam_enthalpy
        self.ice_saturation = property_data.ice_saturation
        self.steam_saturation = property_data.steam_saturation
        self.ice_relperm = property_data.ice_relperm
        self.steam_relperm = property_data.steam_relperm
        self.ice_density = property_data.ice_density
        self.steam_density = property_data.steam_density
        self.ice_viscosity = property_data.ice_viscosity
        self.steam_viscosity = property_data.steam_viscosity

    def evaluate(self, state, values):
        ice_den = self.ice_density.evaluate(state)
        steam_den = self.steam_density.evaluate(state)
        ice_sat = self.ice_saturation.evaluate(state)
        steam_sat = self.steam_saturation.evaluate(state)
        ice_rp  = self.ice_relperm.evaluate(state)
        steam_rp  = self.steam_relperm.evaluate(state)
        ice_vis = self.ice_viscosity.evaluate(state)
        steam_vis = self.steam_viscosity.evaluate(state)
        ice_enth = self.ice_enthalpy.evaluate(state)
        steam_enth = self.steam_enthalpy.evaluate(state)
        temp = self.temperature.evaluate(state)

        total_density = ice_sat * ice_den + steam_sat * steam_den

        # ice volumetric rate
        values[0] = ice_sat * (steam_den * steam_rp / steam_vis) / total_density
        # steam volumetric rate
        values[1] = steam_sat * (steam_den * steam_rp / steam_vis) / total_density
        # temperature
        values[2] = temp
        # energy rate
        values[3] = steam_enth * steam_den * steam_rp / steam_vis

        return 0
    

class geothermal_mass_rate_custom_evaluator_python(operator_set_evaluator_iface):
    def __init__(self, property_data):
        super().__init__()

        self.ice_density = property_data.ice_density
        self.steam_density = property_data.steam_density
        self.ice_saturation = property_data.ice_saturation
        self.steam_saturation = property_data.steam_saturation
        self.ice_relperm = property_data.ice_relperm
        self.steam_relperm = property_data.steam_relperm		
        self.ice_viscosity = property_data.ice_viscosity
        self.steam_viscosity = property_data.steam_viscosity
        self.temperature = property_data.temperature
        self.ice_enthalpy = property_data.ice_enthalpy
        self.steam_enthalpy = property_data.steam_enthalpy

    def evaluate(self, state, values):
        ice_den = self.ice_density.evaluate(state)
        steam_den = self.steam_density.evaluate(state)
        ice_sat = self.ice_saturation.evaluate(state)
        steam_sat = self.steam_saturation.evaluate(state)
        ice_rp  = self.ice_relperm.evaluate(state)
        steam_rp  = self.steam_relperm.evaluate(state)
        ice_vis = self.ice_viscosity.evaluate(state)
        steam_vis = self.steam_viscosity.evaluate(state)
        temp = self.temperature.evaluate(state)
        ice_enth = self.ice_enthalpy.evaluate(state)
        steam_enth = self.steam_enthalpy.evaluate(state)

        total_density = ice_sat * ice_den + steam_sat * steam_den

        # ice mass rate
        values[0] =  steam_den * steam_rp / steam_vis
        # steam mass rate
        values[1] = steam_sat * (ice_den * ice_rp / ice_vis + steam_den * steam_rp / steam_vis) / total_density
        # temperature
        values[2] = temp
        # energy rate
        values[3] = ice_enth * ice_den * ice_rp / ice_vis + steam_enth * steam_den * steam_rp / steam_vis
        
        return 0

