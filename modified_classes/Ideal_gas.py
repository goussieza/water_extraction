from modified_classes.regression_functions import h_steam_linear

class Ideal_gas:
    def __init__(self, T, P, M=18.015, R=8.314):
        """Initializes the gas with pressure P (in MPa), temperature T (in Kelvin), 
       , molar mass M (in g/mol), and gas constant R (in J/mol·K).
       
         M=18.015 g/mol for water, and R=8.314 J/mol·K."""
        
        self.P = P * 1e6 # Pressure bar to pascal
        self.T = T  # Temperature in Kelvin
        self.M = M * 1e-3  # Molar mass in g/mol to kg/mol
        self.R = R  # Gas constant
        
    @property
    def rho(self):
        """ Return the density rho (in kg/m^3)"""
        return (self.P*self.M) / (self.R*self.T)
    
    @property
    def h(self):
        """ Return the specific enthalpy (in kJ/kg)"""        
        return h_steam_linear(self.T)
    @property
    def isothermal_compr(self):
        return (self.rho * self.R * self.T)/(self.M * self.P**2)
    
    

        