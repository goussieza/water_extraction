from darts.engines import property_evaluator_iface

from modified_classes.Water_properties import Water_properties
from modified_classes.Ideal_gas import Ideal_gas
from iapws import _Sublimation_Pressure, _Ice
from modified_classes.regression_functions import T_from_h_ice
from iapws._iapws import _D2O_Viscosity, _Viscosity

class water_density_property_evaluator(property_evaluator_iface):
    def __init__(self):
        super().__init__()
    def evaluate(self, state):
        if Water_properties(state).steam_saturation == 0:
            return Water_properties(state).supercooled_ice_density / 18.015
        if Water_properties(state).steam_saturation == 1:
            return 0.0
        else:
            return Water_properties(state).sublimating_ice_density / 18.015


class temperature_region1_evaluator(property_evaluator_iface):
    def __init__(self):
        super().__init__()
    def evaluate(self, state):
        h=state[1]/18.015 
        return T_from_h_ice(h)

class iapws_enthalpy_region1_evaluator(property_evaluator_iface):
    def __init__(self, temp):
        #super().__init__()
        self.temperature = temp
    def evaluate(self, state):
        P=state[0]*0.1
        return _Ice(self.temperature, P) * 18.015        #kJ/kmol

class iapws_viscosity_evaluator(property_evaluator_iface):
    def __init__(self):
        super().__init__()
    def evaluate(self, state):
        density=iapws_water_density_evaluator()
        den=density.evaluate(state)
        temperature=iapws_temperature_evaluator()
        temp=temperature.evaluate(state)
        return (_Viscosity(den, temp)*1000)



#====================================== Properties for Region 1 and 4 ============================================ 
class iapws_total_enthalpy_evalutor(property_evaluator_iface):
    def __init__(self, temp):
        super().__init__()
        self.T = temp
    def evaluate(self, state):
        P = state[0]*0.1
        P_sub =_Sublimation_Pressure(self.T)    
        if P > P_sub:
            h =_Ice(T=self.T,P=P)['h']
        else:
            h = Ideal_gas(self.T, P=P).h
        return h*18.015


class iapws_temperature_evaluator(property_evaluator_iface):
    def __init__(self):
        super().__init__()
    def evaluate(self, state):
        return Water_properties(state).T

class iapws_water_enthalpy_evaluator(property_evaluator_iface):
    def __init__(self):
        super().__init__()
    def evaluate(self, state):
        if Water_properties(state).steam_saturation == 0:
            return Water_properties(state).h * 18.015 #kJ/kmol
        if Water_properties(state).steam_saturation == 1:
            return 0.0
        else:
            return Water_properties(state).h_sub * 18.015


class iapws_steam_enthalpy_evaluator(property_evaluator_iface):
    def __init__(self):
        super().__init__()
    def evaluate(self, state):
        if Water_properties(state).steam_saturation == 0:
            return 0.0
        if Water_properties(state).steam_saturation == 1:
            return Water_properties(state).h * 18.015
        else:
            return Water_properties(state).h_sat * 18.015


class iapws_water_saturation_evaluator(property_evaluator_iface):
    def __init__(self):
        super().__init__()
    def evaluate(self, state):
        return 1 - Water_properties(state).steam_saturation

class iapws_steam_saturation_evaluator(property_evaluator_iface):
    def __init__(self):
        super().__init__()
    def evaluate(self, state):
        return Water_properties(state).steam_saturation

class iapws_water_relperm_evaluator(property_evaluator_iface):
    def __init__(self):
        super().__init__()
    def evaluate(self, state):
        return 0.0


class iapws_steam_relperm_evaluator(property_evaluator_iface):
    def __init__(self):
        super().__init__()
    def evaluate(self, state):
        return Water_properties(state).steam_saturation


class iapws_water_density_evaluator(property_evaluator_iface):
   def __init__(self):
       super().__init__()
   def evaluate(self, state):
       if Water_properties(state).steam_saturation == 0:
           return Water_properties(state).supercooled_ice_density / 18.015
       if Water_properties(state).steam_saturation == 1:
           return 0.0
       else:
           return Water_properties(state).sublimating_ice_density / 18.015


class iapws_steam_density_evaluator(property_evaluator_iface):
    def __init__(self):
        super().__init__()
    def evaluate(self, state):
        if Water_properties(state).steam_saturation == 0:
            return 0.0
        if Water_properties(state).steam_saturation == 1:
            return Water_properties(state).overheated_steam_density /18.015
        else:
            return Water_properties(state).saturated_steam_density /18.015

class DensitySimple(property_evaluator_iface):
    def __init__(self, dens0, compr=0., p0=1.):
        super().__init__()
        self.dens0 = dens0
        self.compr = compr
        self.p0 = p0

    def evaluate(self, state):
        pressure = state[0] * 0.1
        return self.dens0 * (1 + self.compr * (pressure - self.p0))



class iapws_water_viscosity_evaluator(property_evaluator_iface):
    def __init__(self):
        super().__init__()
    def evaluate(self, state):
        return 1e99



class iapws_steam_viscosity_evaluator(property_evaluator_iface):
    def __init__(self):
        super().__init__()
    def evaluate(self, state):
        return abs(Water_properties(state).steam_viscosity) *1000

