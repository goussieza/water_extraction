from darts.engines import redirect_darts_output

from model_v8 import Model
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator

import numpy as np

redirect_darts_output('run.log')

# Plot pressure profile

def show_all(X, xx, yy, nc, nb, axes, nx, nz):
    ist = 0
    temp = np.array(m.compute_temperature(X))
    vap = np.array(m.compute_steam_quality(X, nb))
    p = np.array(X[0:nc * nb:nc])
    h = np.array(X[1:nc * nb:nc])

    axes[0][0].set_ylabel('Pressure, bar')
    pos = axes[0][0].pcolormesh(xx, yy, p.reshape(nz, nx))
    plt.colorbar(pos, ax=axes[0][0])

    axes[1][0].set_ylabel('Temperature, K')
    pos = axes[1][0].pcolormesh(xx, yy, temp.reshape(nz, nx))
    plt.colorbar(pos, ax=axes[1][0])

    axes[1][1].set_ylabel('Vapor Quality')
    pos = axes[1][1].pcolormesh(xx, yy, vap.reshape(nz, nx))
    plt.colorbar(pos, ax=axes[1][1])

    axes[0][1].set_ylabel('Specific Enthalpy, kJ/kg')
    pos = axes[0][1].pcolormesh(xx, yy, h.reshape(nz, nx))
    plt.colorbar(pos, ax=axes[0][1])


def plot_all(x, X, nc, nb, axes, legend):
    ist = 0
    temp = m.compute_temperature(X)
    vap = m.compute_steam_saturation(X, nb)

    axes[0][0].plot(x[ist:], X[0:nc * nb:nc][ist:])
    axes[0][0].set_ylabel('Pressure, bar')

    axes[1][0].plot(x[ist:], temp[ist:])
    axes[1][0].set_ylabel('Temperature, K')

    # axes[1][1].axhline(y=1, color='grey', linestyle='-',linewidth=0.5)
    axes[1][1].axhline(y=0, color='grey', linestyle='-', linewidth=0.5)
    axes[1][1].plot(x[ist:], vap[ist:])
    axes[1][1].set_ylabel('Vapor Quality')

    axes[0][1].plot(x[ist:], X[1:nc * nb:nc][ist:], label=legend)
    axes[0][1].set_ylabel('Specific Enthalpy, kJ/kg')
    axes[0][1].legend(loc='upper left', bbox_to_anchor=(1, 1))

    for ax in axes.ravel():
        ax.xaxis.set_minor_locator(MultipleLocator(10))
        ax.grid(True, which='both', axis='x', linestyle='-', linewidth=0.5)


m = Model(T0=260, uniform_temperature=210, uniform_pressure=1e-5, p0=1e-4, n_points=4000)
m.init()
nb = m.reservoir.nb
nc = 2
(nx, ny, nz) = (m.reservoir.nx, m.reservoir.ny, m.reservoir.nz)
indx = []
for k in range(int(2*nz/5), int(3*nz/5)):
    indx.append(1+k*2*nx*ny)

X = np.array(m.physics.engine.X, copy=False)
x = np.linspace(0, nx * m.dx, nx+1)
y = np.linspace(nz * m.dz, 0, nz+1)
xx, yy = np.meshgrid(x, y)


fig, axes = plt.subplots(2, 2, figsize=(5, 8))
  
t_tot = 0
heat_rate = -2e3
#plot_all(x, X, nc, nb, axes, f't = {t_tot} days rhs on')
m.set_rhs_flux(inflow_var_idx=indx, outflow=heat_rate/nz/2)
for i in range(1):
    dt = 2
    m.run_python(dt)

    t_tot += dt
    #plot_all(x, X, nc, nb, axes, f't = {t_tot} days rhs on')

show_all(X, xx, yy, nc, nb, axes, nx, nz)

# fig, axes = plt.subplots(2, 2, figsize=(12, 10))
# m.reservoir.wells[0].control = m.physics.new_bhp_prod(1e-5)
# m.set_rhs_flux(inflow_var_idx=0, outflow=-0e0)
# for i in range(0):
#     dt = 40
#     m.run_python(dt)
#     t_tot += dt
#     show_all(X, nc, nb, axes, nx, ny, nz)




# plt.suptitle('P0 = 1e-4 bar, T0 = 260 K, P_atm= 1e-5 bar, T_atm= 200K\n Phase change but 2 last cells are set at atm condition')
plt.tight_layout()


time_data = pd.DataFrame.from_dict(m.physics.engine.time_data)
writer = pd.ExcelWriter('time_data.xlsx')
time_data.to_excel(writer, 'Sheet1')
writer.close()

from darts.tools.plot_darts import *
if len(m.reservoir.wells) > 0:
    # string = 'P1 : steam rate'
    # ax1 = time_data.plot(x='time', y=[col for col in time_data.columns if string in col])
    # ax1.tick_params(labelsize=14)
    # ax1.set_xlabel('Days', fontsize=14)
    plot_total_prod_steam_rate_darts(time_data)

# string = 'P1 : water rate'
# ax2 = time_data.plot(x='time', y=[col for col in time_data.columns if string in col])
# ax2.tick_params(labelsize=14)
# ax2.set_xlabel('Days', fontsize=14)

plt.show()