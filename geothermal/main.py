from darts.engines import redirect_darts_output

from model_v4 import Model
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator

import numpy as np

redirect_darts_output('run.log')

# Plot pressure profile

def plot_all(x, X, nc, nb, axes, legend):
    temp = m.compute_temperature(X)
    vap = m.compute_vapor_quality(X, nb)
    
    axes[0][0].plot(x, X[0:nc * nb:nc])
    axes[0][0].set_ylabel('Pressure, bar')   
    
    axes[1][0].plot(x, temp)
    axes[1][0].set_ylabel('Temperature, K')   
    
    
    # axes[1][1].axhline(y=1, color='grey', linestyle='-',linewidth=0.5)
    axes[1][1].axhline(y=0, color='grey', linestyle='-',linewidth=0.5)
    axes[1][1].plot(x[1:], vap[1:])
    axes[1][1].set_ylabel('Vapor Quality')
    
    axes[0][1].plot(x[1:], X[1:nc * nb:nc][1:], label=legend)
    axes[0][1].set_ylabel('Specific Enthalpy, kJ/kg')
    axes[0][1].legend(loc='upper left', bbox_to_anchor=(1, 1))
    
    
    for ax in axes.ravel():
        ax.xaxis.set_minor_locator(MultipleLocator(1))
        ax.grid(True, which='both', axis='x', linestyle='-', linewidth=0.5)


m = Model(T0=260, uniform_temperature=210, uniform_pressure=1e-5, p0=1e-4)
m.init()
nb = m.reservoir.nb
nc = 2
x = np.arange(nb)
X = np.array(m.physics.engine.X, copy=False)
fig, axes = plt.subplots(2, 2, figsize=(15, 10))
  
t_tot = 0
plot_all(x, X, nc, nb, axes, f't = {t_tot} days')
for i in range(5):
    dt = 100/2
    m.run_python(dt, restart_dt=dt)  
    plot_all(x, X, nc, nb, axes, f't = {t_tot} days')

    t_tot += dt



# plt.suptitle('P0 = 1e-4 bar, T0 = 260 K, P_atm= 1e-5 bar, T_atm= 200K\n Phase change but 2 last cells are set at atm condition')
plt.tight_layout()
plt.show()


   

