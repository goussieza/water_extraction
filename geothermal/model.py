from darts.models.reservoirs.struct_reservoir import StructReservoir
from darts.models.darts_model import DartsModel, sim_params
import numpy as np
from darts.engines import value_vector

from modified_classes.geothermal_custom import Geothermal_custom
from modified_classes.physics import Geothermal
from modified_classes.iapws_property import iapws_total_enthalpy_evalutor
from modified_classes.regression_functions import h_steam, T_from_h_steam
from iapws import _Ice 


class Model(DartsModel):
    def __init__(self, n_points=128):
        # call base class constructor
        super().__init__()

        self.timer.node["initialization"].start()

        (nx, ny, nz) = (10, 1, 1)
        nb = nx * ny * nz
        perm = np.ones(nb) * 2000
        #perm = load_single_keyword('permXVanEssen.in', 'PERMX')
        #perm = perm[:nb]

        poro = np.ones(nb) * 0.2
        self.dx = 10
        self.dy = 1
        dz = np.ones(nb) * 1

        # discretize structured reservoir
        self.reservoir = StructReservoir(self.timer, nx=nx, ny=ny, nz=nz, dx=self.dx, dy=self.dy, dz=dz, permx=perm,
                                         permy=perm, permz=perm * 0.1, poro=poro, depth=2000)

        self.reservoir.set_boundary_volume(yz_minus=1e15, yz_plus=1e15)



        # add well
        # self.reservoir.add_well("INJ")
        # n_perf = nz
        # # add perforations to te payzone
        # for n in range(0, n_perf):
        #     self.reservoir.add_perforation(well=self.reservoir.wells[-1], i=1, j=1, k=n + 1,
        #                                    well_radius=0.16)
        #
        # # add well
        # self.reservoir.add_well("PRD")
        # # add perforations to te payzone
        # for n in range(0, n_perf):
        #     self.reservoir.add_perforation(self.reservoir.wells[-1], nx, 1, n + 1, 0.16)

        # rock heat capacity and rock thermal conduction
        self.hcap = np.array(self.reservoir.mesh.heat_capacity, copy=False)
        self.rcond = np.array(self.reservoir.mesh.rock_cond, copy=False)
        self.hcap.fill(0)
        self.rcond.fill(0)

        # create pre-defined physics for geothermal
        self.physics = Geothermal(self.timer, n_points, min_p=1e-15, max_p=1,
                                  min_e=_Ice(T=50, P=3e-15)['h']*18.015, max_e=h_steam(273)*18.015, cache=False)
        # self.physics = Geothermal_custom(self.timer, n_points=100, min_p=1e-5, max_p=6e-3,
        #                             min_e=-600 * 18.015, max_e=2800 * 18.015)

        self.params.first_ts = 1e-3
        self.params.mult_ts = 8
        self.params.max_ts = 10

        # Newton tolerance is relatively high because of L2-norm for residual and well segments
        self.params.tolerance_newton = 1e-2
        self.params.tolerance_linear = 1e-6
        self.params.max_i_newton = 20
        self.params.max_i_linear = 40

        self.params.newton_type = sim_params.newton_global_chop
        self.params.newton_params = value_vector([1])

        self.runtime = 3650
        # self.physics.engine.silent_mode = 0
        self.timer.node["initialization"].stop()

    def set_initial_conditions(self):
        self.physics.set_uniform_initial_conditions(self.reservoir.mesh, uniform_pressure=3e-5,
                                                    uniform_temperature=250)

        mesh = self.reservoir.mesh
        pressure = np.array(mesh.pressure, copy=False)
        pressure[0] = 3e-4

        state = value_vector([pressure[0], 0])
        E = iapws_total_enthalpy_evalutor(260)
        enth = E.evaluate(state)

        enthalpy = np.array(mesh.enthalpy, copy=False)
        enthalpy[0] = enth


    def set_boundary_conditions(self):
        for i, w in enumerate(self.reservoir.wells):
            if i == 0:
                #w.control = self.physics.new_rate_water_inj(8000, 300)
                w.control = self.physics.new_bhp_water_inj(230, 308.15)
            else:
                #w.control = self.physics.new_rate_water_prod(8000)
                w.control = self.physics.new_bhp_prod(180)

    def compute_temperature(self, X):
        from modified_classes.Water_properties import Water_properties

        nb = self.reservoir.mesh.n_res_blocks
        h_list=X[1:2 * nb:2]
        p_list=X[0:2 * nb:2] 

        temp=[Water_properties([p,h]).T for p, h in zip(p_list, h_list)]
        return temp
    
    def compute_saturation(self,X,nb):
        from modified_classes.Water_properties import Water_properties
        p_list=X[0:2 * nb:2] 
        h_list=X[1:2 * nb:2]
        ss_list=[Water_properties([p,h]).steam_saturation for p, h in zip(p_list, h_list)]
        return ss_list
    
    def molar_h_to_spec(self,X,nb):
        h_list=X[1:2 * nb:2]
        h_spec_list=[h/18.015 for h in h_list]
        return h_spec_list
    
    def compute_steam_density(self,X, nb):
        from modified_classes.Water_properties import Water_properties
        p_list = X[0:2 * nb:2] 
        h_list = X[1:2 * nb:2]
        ss_list = self.compute_saturation(X, nb)

        density_list = []

        for p, h, ss in zip(p_list, h_list, ss_list):
            if ss == 1:
                density_list.append(Water_properties([p, h]).overheated_steam_density)  # Replace method1 with actual method name
            elif ss == 0:
                density_list.append(0)
            else:
                density_list.append(Water_properties([p, h]).saturated_steam_density)  # Replace method2 with actual method name

        return density_list
    
    def compute_vapor_quality(self,X,nb):
        from modified_classes.Water_properties import Water_properties
        p_list=X[0:2 * nb:2] 
        h_list=X[1:2 * nb:2]
        vp_list=[Water_properties([p,h]).vapor_quality for p, h in zip(p_list, h_list)]
        return vp_list
        
        

    def set_op_list(self):
        self.op_list = [self.physics.acc_flux_itor, self.physics.acc_flux_itor_well]
        op_num = np.array(self.reservoir.mesh.op_num, copy=False)
        op_num[self.reservoir.mesh.n_res_blocks:] = 1

    def export_pro_vtk(self, file_name='Results'):
        X = np.array(self.physics.engine.X, copy=False)
        nb = self.reservoir.mesh.n_res_blocks
        temp = _Backward1_T_Ph_vec(X[0:2 * nb:2] / 10, X[1:2 * nb:2] / 18.015)
        local_cell_data = {'Temperature': temp,
                           'Perm': self.reservoir.global_data['permx'][self.reservoir.discretizer.local_to_global]}

        self.export_vtk(file_name, local_cell_data=local_cell_data)
        
        
    
