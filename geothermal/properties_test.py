from darts.engines import redirect_darts_output

from model_v2 import Model
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

redirect_darts_output('run.log')

def plot_all(x, X, nc, nb,label):
    temp = m.compute_temperature(X)
    temp2= m.compute_temperature_2(X)
    ss=m.compute_saturation(X,nb)
    spec_h=m.molar_h_to_spec(X, nb)
    density=m.compute_steam_density(X, nb)
    vp=m.compute_vapor_quality(X, nb)
        
    
    axes[0][0].semilogy(x, X[0:nc * nb:nc])
    axes[0][0].set_ylabel('Pressure, bar')
    
    axes[0][1].plot(x, spec_h)
    axes[0][1].set_ylabel('Specific Enthalpy, kJ/kg')
    
    # axes[0][2].plot(x, vp)
    # axes[0][2].set_ylabel('Vapor Quality')
    
    
    axes[1][0].plot(x, temp)
    axes[1][0].set_ylabel('Temperature using IAPWS_property.py, K')
    
    axes[1][1].plot(x, temp2)
    axes[1][1].set_ylabel('Temperature using directly Water_properties, K')
    
    # axes[1][2].plot(x, density,label=label)
    # axes[1][2].set_ylabel('Steam Density, kg/m3')

#Single phase steam, different dt###
p0=3e-4
T0=260
p_atm=3e-5
T_atm=250
m = Model(p0=p0,T0=T0, uniform_pressure=p_atm, uniform_temperature=T_atm)

m.init()

nb = m.reservoir.nb
nc = 2
x = np.arange(nb)
X = np.array(m.physics.engine.X, copy=False)
fig, axes = plt.subplots(2, 2, figsize=(15, 10))

dt=[1000,2000,3000,4000,5000]

for t in dt:
    m.run_python(t)
    plot_all(x, X, nc, nb, f'dt = {t}')
    
fig.legend(loc='center right', bbox_to_anchor=(1.1, 0.5))
plt.suptitle('Single phase steam: P_atm = 3e-5 bar ; T_atm = 250K ; P0 = 3e-4 bar ; T0 = 260K \n \n dt test')
plt.tight_layout()
plt.show()

# #Single phase steam, different hcap###
# p0=3e-4
# T0=260
# p_atm=3e-5
# T_atm=250
# m = Model(p0=p0,T0=T0, uniform_pressure=p_atm, uniform_temperature=T_atm)

# m.init()

# nb = m.reservoir.nb
# nc = 2
# x = np.arange(nb)
# X = np.array(m.physics.engine.X, copy=False)
# fig, axes = plt.subplots(2, 3, figsize=(15, 10))

# hcap=[0,1,10,100,1000]

# for cap in hcap:
#     m.hcap.fill(cap)
#     m.run_python(1000)
#     plot_all(x, X, nc, nb, f'hcap = {cap}')
    
# fig.legend(loc='center right', bbox_to_anchor=(1.1, 0.5))
# plt.suptitle('Single phase steam: P_atm = 3e-5 bar ; T_atm = 250K ; P0 = 3e-4 bar ; T0 = 260K \n \n hcap test')
# plt.tight_layout()
# plt.show()


# #Single phase steam, different hcap###
# p0=3e-4
# T0=260
# p_atm=3e-5
# T_atm=250
# m = Model(p0=p0,T0=T0, uniform_pressure=p_atm, uniform_temperature=T_atm)

# m.init()

# nb = m.reservoir.nb
# nc = 2
# x = np.arange(nb)
# X = np.array(m.physics.engine.X, copy=False)
# fig, axes = plt.subplots(2, 3, figsize=(15, 10))

# rcond=[0,1,10,100,1000]

# for cond in rcond:
#     m.rcond.fill(cond)
#     m.run_python(1000)
#     plot_all(x, X, nc, nb, f'rcond = {cond}')
    
# fig.legend(loc='center right', bbox_to_anchor=(1.1, 0.5))
# plt.suptitle('Single phase steam: P_atm = 3e-5 bar ; T_atm = 250K ; P0 = 3e-4 bar ; T0 = 260K \n \n rcond test')
# plt.tight_layout()
# plt.show()

# #Single phase steam, different dt, moon pressure###
# p0=3e-4
# T0=260
# p_atm=3e-10
# T_atm=250
# m = Model(p0=p0,T0=T0, uniform_pressure=p_atm, uniform_temperature=T_atm)

# m.init()

# nb = m.reservoir.nb
# nc = 2
# x = np.arange(nb)
# X = np.array(m.physics.engine.X, copy=False)
# fig, axes = plt.subplots(2, 3, figsize=(15, 10))

# dt=[1000,2000,3000,4000,5000]

# for t in dt:
#     m.run_python(t)
#     plot_all(x, X, nc, nb, f'dt = {t}')
    
# fig.legend(loc='center right', bbox_to_anchor=(1.1, 0.5))
# plt.suptitle('Moon pressure condition: P_atm = 3e-10 bar ; T_atm = 250K ; P0 = 3e-4 bar ; T0 = 260K')
# plt.tight_layout()
# plt.show()

# #Differents T_atm###
# p0=3e-4
# T0=260
# p_atm=3e-10
# T_atms=[160,155,150,145,140,135]

# fig, axes = plt.subplots(2, 3, figsize=(15, 10))

# for T_atm in T_atms:
#     m = Model(p0=p0, T0=T0, uniform_pressure=p_atm, uniform_temperature=T_atm)
#     m.init()
#     nb = m.reservoir.nb
#     nc = 2
#     x = np.arange(nb)
#     X = np.array(m.physics.engine.X, copy=False)
#     dt = 1000
#     m.run_python(dt)
#     plot_all(x, X, nc, nb, f'T_atm = {T_atm}K') 

# fig.legend(loc='center right', bbox_to_anchor=(1.1, 0.5))
# plt.suptitle('Moon pressure condition,Temperature close to sublimation curve, phase change occur: P_atm = 3e-10 bar ; P0 = 3e-4 bar ; T0 = 260K')
# plt.tight_layout()
# plt.show()

# #Differents T_atm###
# p0=3e-4
# T0=260
# p_atm=3e-10
# T_atms=[135,118,117,116,115,75]

# fig, axes = plt.subplots(2, 3, figsize=(15, 10))

# for T_atm in T_atms:
#     m = Model(p0=p0, T0=T0, uniform_pressure=p_atm, uniform_temperature=T_atm)
#     m.init()
#     nb = m.reservoir.nb
#     nc = 2
#     x = np.arange(nb)
#     X = np.array(m.physics.engine.X, copy=False)
#     dt = 1000
#     m.run_python(dt)
#     plot_all(x, X, nc, nb, f'T_atm = {T_atm}K') 

# fig.legend(loc='center right', bbox_to_anchor=(1.1, 0.5))
# plt.suptitle('Moon pressure condition,Temperature less close to sublimation curve, phase change occur: P_atm = 3e-10 bar ; P0 = 3e-4 bar ; T0 = 260K')
# plt.tight_layout()
# plt.show()
