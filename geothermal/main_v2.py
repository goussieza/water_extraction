from darts.engines import redirect_darts_output

from model_v5 import Model
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator

import numpy as np

redirect_darts_output('run.log')

# Plot pressure profile

def plot_all(x, X, nc, nb, axes, legend):
    ist = 0
    temp = m.compute_temperature(X)
    vap = m.compute_steam_quality(X, nb)
    
    axes[0][0].plot(x[ist:], X[0:nc * nb:nc][ist:], label=legend)
    axes[0][0].set_ylabel('Pressure, bar')   
    
    axes[1][0].plot(x[ist:], temp[ist:])
    axes[1][0].set_ylabel('Temperature, K')   
    
    
    # axes[1][1].axhline(y=1, color='grey', linestyle='-',linewidth=0.5)
    #axes[1][1].axhline(y=0, color='grey', linestyle='-', linewidth=0.5)
    axes[1][1].plot(x[ist:], vap[ist:])
    axes[1][1].set_ylabel('Vapor Quality')
    
    axes[0][1].plot(x[ist:], X[1:nc * nb:nc][ist:])
    axes[0][1].set_ylabel('Specific Enthalpy, kJ/kg')

    axes[0][0].legend(loc='upper right')
    
    
    for ax in axes.ravel():
        ax.xaxis.set_minor_locator(MultipleLocator(5))
        ax.grid(True, which='both', axis='x', linestyle='-', linewidth=0.5)


m = Model(T0=260, uniform_temperature=210, uniform_pressure=1e-5, p0=1e-4, n_points=4000)
m.init()
nb = m.reservoir.nb
nc = 2
(nx, ny, nz) = (m.reservoir.nx, m.reservoir.ny, m.reservoir.nz)
x = np.linspace(0, nx * m.dx, nx)
X = np.array(m.physics.engine.X, copy=False)
fig, axes1 = plt.subplots(2, 2, figsize=(12, 10))
  
t_tot = 0
plot_all(x, X, nc, nb, axes1, f't = {t_tot} days')

heat_rate = -2e3
m.set_rhs_flux(inflow_var_idx=1, outflow=heat_rate)
for i in range(5):
    dt = .3
    m.run_python(dt)
    t_tot += dt
    plot_all(x, X, nc, nb, axes1, f't = {round(t_tot, 2)} days')

fig, axes2 = plt.subplots(2, 2, figsize=(12, 10))
plot_all(x, X, nc, nb, axes2, f't = {t_tot} days')

heat_rate = 0
m.set_rhs_flux(inflow_var_idx=1, outflow=heat_rate)
for i in range(5):
    dt = 8
    m.params.max_ts = 0.1
    m.run_python(dt)
    t_tot += dt
    plot_all(x, X, nc, nb, axes2, f't = {round(t_tot, 2)} days')

# plt.suptitle('P0 = 1e-4 bar, T0 = 260 K, P_atm= 1e-5 bar, T_atm= 200K\n Phase change but 2 last cells are set at atm condition')
plt.tight_layout()


time_data = pd.DataFrame.from_dict(m.physics.engine.time_data)
writer = pd.ExcelWriter('time_data.xlsx')
time_data.to_excel(writer, 'Sheet1')
writer.close()

from darts.tools.plot_darts import *
if len(m.reservoir.wells) > 0:
    # string = 'P1 : steam rate'
    # ax1 = time_data.plot(x='time', y=[col for col in time_data.columns if string in col])
    # ax1.tick_params(labelsize=14)
    # ax1.set_xlabel('Days', fontsize=14)
    plot_total_prod_steam_rate_darts(time_data)

# string = 'P1 : water rate'
# ax2 = time_data.plot(x='time', y=[col for col in time_data.columns if string in col])
# ax2.tick_params(labelsize=14)
# ax2.set_xlabel('Days', fontsize=14)

plt.show()