from darts.models.reservoirs.struct_reservoir import StructReservoir
from darts.models.darts_model import DartsModel, sim_params
import numpy as np
from darts.engines import value_vector

from darts.physics.geothermal.physics import Geothermal
from darts.physics.geothermal.property_container import PropertyContainer

from darts.physics.properties.iapws.custom_rock_property import *

from modified_classes.regression_functions import h_steam_linear
from iapws import _Ice
from darts.physics.properties.basic import ConstFunc

from modified_classes.iapws_property import *

class Model(DartsModel):
    def __init__(self, p0=1e-4, T0=260, uniform_pressure=1e-5, uniform_temperature=250, n_points=128):
        # call base class constructor
        super().__init__()

        self.timer.node["initialization"].start()

        (self.nx, self.ny, self.nz) = (40, 1, 20)
        nb = self.nx * self.ny * self.nz
        perm = np.ones(nb) * 5000
        # x_axes = np.logspace(-3, 1, self.nx)
        # print(sum(x_axes))
        # self.dx = np.tile(x_axes, self.nz)
        self.dx = 1
        self.dy = 1
        self.dz = 1

        poro = np.ones(nb) * 0.75

        self.depth = np.zeros(nb)
        n_layer = self.nx * self.ny
        for k in range(self.nz):
            self.depth[k*n_layer:(k+1)*n_layer] = 20 + k * self.dz

        # discretize structured reservoir
        self.reservoir = StructReservoir(self.timer, nx=self.nx, ny=self.ny, nz=self.nz,
                                         dx=self.dx, dy=self.dy, dz=self.dz,
                                         permx=perm, permy=perm, permz=perm*0.1, poro=poro, depth=2000)

        #self.reservoir.set_boundary_volume(yz_plus=1e8)
        self.reservoir.add_well("P1")
        for i in range(self.nx):
            self.reservoir.add_perforation(self.reservoir.wells[-1], i+1, 1, 5, well_index=1e8)


        # rock heat capacity and rock thermal conduction
        hcap = np.array(self.reservoir.mesh.heat_capacity, copy=False)
        rcond = np.array(self.reservoir.mesh.rock_cond, copy=False)
        hcap.fill(2050)
        rcond.fill(0)

        property = PropertyContainer()

        property.temperature = iapws_temperature_evaluator()  # Create temperature object
        property.water_enthalpy = iapws_water_enthalpy_evaluator()  # Create water_enthalpy object
        property.steam_enthalpy = iapws_steam_enthalpy_evaluator()  # Create steam_enthalpy object
        property.total_enthalpy = iapws_total_enthalpy_evalutor
        property.water_saturation = iapws_water_saturation_evaluator()  # Create water_saturation object
        property.steam_saturation = iapws_steam_saturation_evaluator()  # Create steam_saturation object
        property.water_relperm = ConstFunc(0)  # Create water_relperm object
        property.steam_relperm = ConstFunc(1)  # Create steam_relperm object
        property.water_density = iapws_water_density_evaluator()  # Create water_density object
        property.steam_density = DensitySimple(p0=p0, dens0=1e-3, compr=1e-3)  # Create steam_density object
        property.water_viscosity = ConstFunc(1)  # Create water_viscosity object
        property.steam_viscosity = ConstFunc(1)  # Create steam_viscosity object
        property.water_conduction = ConstFunc(17.8)  # ice thermal conduction
        property.steam_conduction = ConstFunc(0)  # steam thermal conduction

        rock = [value_vector([1e-5, 1e-3, uniform_temperature])]
        property.rock_compaction = custom_rock_compaction_evaluator(rock)  # Create rock_compaction object
        property.rock_energy = custom_rock_energy_evaluator(rock)  # Create rock_energy object

        # create pre-defined physics for geothermal
        min_e = _Ice(T=uniform_temperature-40, P=3e-15)['h']*18.015
        max_e = h_steam_linear(T0+20)*18.015
        print(min_e, max_e)
        self.physics = Geothermal(self.timer, n_points=10000, min_p=1e-15, max_p=1,
                                  min_e=min_e, max_e=max_e, cache=False)
        self.physics.add_property_region(property_container=property)

        self.physics.init_physics()

        self.params.first_ts = 1e-5
        self.params.mult_ts = 2
        self.params.max_ts = 1e-1

        # Newton tolerance is relatively high because of L2-norm for residual and well segments
        self.params.tolerance_newton = 1e-2
        self.params.tolerance_linear = 1e-6
        self.params.max_i_newton = 20
        self.params.max_i_linear = 40

        self.params.newton_type = sim_params.newton_global_chop
        self.params.newton_params = value_vector([1])

        self.runtime = 3650
        # self.physics.engine.silent_mode = 0
        self.timer.node["initialization"].stop()

        self.p0 = p0
        self.T0 = T0
        self.uniform_pressure = uniform_pressure
        self.uniform_temperature = uniform_temperature
        
        
    def set_rhs_flux(self, inflow_var_idx: int, outflow: float):
        '''
        function to specify the inflow or outflow to the cells
        it sets up self.rhs_flux vector on nvar * ncells size
        which will be added to rhs in darts_model.run_python function
        :param inflow_cells: cell indices where to apply inflow or outflow
        :param inflow_var_idx: variable index [0..nvars-1]
        :param outflow: inflow_var_idx<nc => kMol/day, else kJ/day (thermal var)
        if outflow < 0 then it is actually inflow
        '''
        nv = self.physics.n_vars
        nb = self.reservoir.mesh.n_res_blocks
        self.rhs_flux = np.zeros(nb * nv)
        # extract pointer to values corresponding to var_idx
        self.rhs_flux[inflow_var_idx] = outflow


    def set_initial_conditions(self):
        self.physics.set_uniform_initial_conditions(self.reservoir.mesh, uniform_pressure=self.uniform_pressure,
                                                    uniform_temperature=self.uniform_temperature)

        # mesh = self.reservoir.mesh
        # pressure = np.array(mesh.pressure, copy=False)
        #
        # pressure[0]=self.p0
        # # pressure[0:-1].fill(self.p0)
        # # pressure[0:-50].fill(self.p0)
        #
        #
        # state = value_vector([pressure[0], 0])
        # E = self.physics.property_containers[0].total_enthalpy(self.T0)
        # enth = E.evaluate(state)
        #
        # enthalpy = np.array(mesh.enthalpy, copy=False)
        #
        # enthalpy[0]=enth
        # # enthalpy[0:-1].fill(enth)
        # # enthalpy[0:-50].fill(enth)
        


       


    def set_boundary_conditions(self):
        for i, w in enumerate(self.reservoir.wells):
            w.control = self.physics.new_bhp_prod(1e-6)
            #w.constraint = self.physics.new_rate_water_inj(0)

    def compute_temperature(self, X):
        nb = self.reservoir.mesh.n_res_blocks
        h_list = X[1:2 * nb:2]
        p_list = X[0:2 * nb:2]

        temp = [self.physics.property_containers[0].temperature.evaluate([p, h]) for p, h in zip(p_list, h_list)]
        return temp

    def compute_vapor_quality(self,X,nb):
        from modified_classes.Water_properties import Water_properties
        p_list=X[0:2 * nb:2] 
        h_list=X[1:2 * nb:2]
        vp_list=[Water_properties([p,h]).vapor_quality for p, h in zip(p_list, h_list)]
        return vp_list
    
    def set_op_list(self):
        self.op_list = [self.physics.acc_flux_itor[0], self.physics.acc_flux_w_itor]
        op_num = np.array(self.reservoir.mesh.op_num, copy=False)
        op_num[self.reservoir.mesh.n_res_blocks:] = 1

    def export_pro_vtk(self, file_name='Results'):
        X = np.array(self.physics.engine.X, copy=False)
        nb = self.reservoir.mesh.n_res_blocks
        temp = _Backward1_T_Ph_vec(X[0:2 * nb:2] / 10, X[1:2 * nb:2] / 18.015)
        local_cell_data = {'Temperature': temp,
                           'Perm': self.reservoir.global_data['permx'][self.reservoir.discretizer.local_to_global]}

        self.export_vtk(file_name, local_cell_data=local_cell_data)
