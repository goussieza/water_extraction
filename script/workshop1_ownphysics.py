# -*- coding: utf-8 -*-
"""
Created on Wed Jun  7 09:28:56 2023

@author: Antoine.Goussiez
"""


#STEP1

'''Import all important packages from DARTS installation'''
from darts.engines import *

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


from modified_classes.geothermal_custom import Geothermal_custom
from modified_classes.custom_evaluators import *
from modified_classes.regression_functions import h_steam

redirect_darts_output('run_water_lowpressure.log')

#STEP2

'''Activate main timers for simulation'''
def activate_timer():
    # Call class constructor and Build timer_node object
    timer = timer_node()

    # Call object members; there are 2 types of members:
    ## <1>Function member:
    timer.start()
    ## <2>Data member:
    timer.node["simulation"] = timer_node()
    timer.node["initialization"] = timer_node()

    # Start initialization
    timer.node["initialization"].start()
    
    return timer

#STEP3

'''Define main parameters for simulation by overwriting default parameters'''
def define_params(max_ts=365):
    # Build a sim_params object by calling constructor
    params = sim_params()

    # Adjust time step settings
    # First time step (time unit: day)
    params.first_ts = 0.01

    # Time step multiplier
    params.mult_ts = 2

    # Maximum time step
    params.max_ts = max_ts

    # Newton tolerance
    params.tolerance_newton = 1e-5
    params.tolerance_linear = 1e-8
    params.linear_type = params.cpu_superlu
    
    return params


#STEP4

'''Mesh initialization for 1D reservoir with constant transmissibility'''
def init_mesh(nb):
    # Create mesh object by calling the class constructor
    mesh = conn_mesh()

    # Create connection list for 1D reservoir 
    block_m = np.arange(nb - 1, dtype=np.int32)
    block_p = block_m + 1

    # Set constant transmissbility
    permeability = 2
    tranD = np.ones(nb - 1) * 1e+1 * nb
    tran = tranD * permeability
    # Initialize mesh with connection list
    mesh.init(index_vector(block_m), index_vector(block_p),
              value_vector(tran), value_vector(tranD))

    # Complete mesh initialization
    mesh.reverse_and_sort()
    
    return mesh

#STEP5

#Building a 1*1*1 m3 reservoir

'''Define basic properties for the reservoir'''
def define_reservoir(nb):
    # Create numpy arrays wrapped around mesh data (no copying)
    volume = np.array(mesh.volume, copy=False)
    porosity = np.array(mesh.poro, copy=False)
    depth = np.array(mesh.depth, copy=False)

    # Assign volume, porosity and depth values
    
    #Building a 1*1*1 m3 reservoir

    volume.fill(1/nb)
    porosity.fill(0.5)
    depth.fill(1)

    # Make first and last blocks large (source/sink)
    volume[0] = 1e10
    volume[nb-1] = 1e10
#STEP6


'''Mimic boundary conditions for the reservoir'''
def define_initial_conditions(nb,T_atm,p_atm,p0,T0):
    # Create numpy wrappers for initial solution
    
    state = value_vector([p_atm, 0])
    E = custom_total_enthalpy_evalutor(T_atm)
    enth = E.evaluate(state)
    E0 = custom_total_enthalpy_evalutor(T0)
    enth0= E0.evaluate(state)
    
    pressure = np.array(mesh.pressure, copy=False) 
    enthalpy = np.array(mesh.enthalpy, copy=False)

    # Assign initial pressure values
    pressure.fill(p_atm)
    pressure[0] = p0
    
    print('h_initial(kJ/kg)=', enth/18.015)
    enthalpy.fill(enth)
    # enthalpy.fill(h_atm)
    enthalpy[0] = enth0

#STEP7

'''Create physics from predefined properties from DARTS package'''
def define_physics():
    # basic physical parameters

    # Activate physics
    physics = Geothermal_custom(timer, n_points=100, min_p=1e-5, max_p=6e-3,
                                min_e=-600*18.015, max_e=2800*18.015)
    

    return physics

#STEP8



def compute_saturation(X,nb):
    from modified_classes.Water_properties import Water_properties
    p_list = X[0:2 * nb:2]
    h_list = X[1:2 * nb:2]
    ss_list = [Water_properties([p, h]).steam_saturation for p, h in zip(p_list, h_list)]
    return ss_list
    
def molar_h_to_spec(X,nb):
    return X[1:2 * nb:2]/18.015

def states_list(X,nb):
    p_list = X[0:2 * nb:2]
    h_list = X[1:2 * nb:2]
    states_list = list(zip(p_list, h_list))
    return states_list

def temp_to_enth_steam(temp):
    h = [h_steam(t) for t in temp]
    return h

def compute_steam_density(X, nb):
    from modified_classes.Water_properties import Water_properties
    p_list = X[0:2 * nb:2] 
    h_list = X[1:2 * nb:2]
    ss_list = compute_saturation(X, nb)

    density_list = []

    for p, h, ss in zip(p_list, h_list, ss_list):
        if ss == 1:
            density_list.append(Water_properties([p, h]).overheated_steam_density)  # Replace method1 with actual method name
        elif ss == 0:
            density_list.append(0)
        else:
            density_list.append(Water_properties([p, h]).saturated_steam_density)  # Replace method2 with actual method name

    return density_list

# def plot_profile(data, name, ax):
#     n = len(data)
#     ax.plot(np.arange(n), data[0:n], '-')
#     ax.set_xlabel('Grid index')
#     ax.set_ylabel('%s' % (name))  # Get numpy wrapper for final solution

def compute_temperature(X,nb):
    from modified_classes.Water_properties import Water_properties
    p_list = X[0:2 * nb:2]
    h_list = X[1:2 * nb:2]
    T_list = [custom_temperature_evaluator.evaluate(custom_temperature_evaluator, state=[p, h])
              for p, h in zip(p_list, h_list)]
    return T_list
        
'''RUN1 : Single phase steam'''
nb = 5
timer = activate_timer()
params = define_params(max_ts=1e4)
mesh = init_mesh(nb)
define_reservoir(nb)
define_initial_conditions(nb, T_atm=250, p_atm=3e-5, T0=260, p0=3e-4)
physics = define_physics()

# Initialize engine
physics.engine.init(mesh, ms_well_vector(),
                    op_vector([physics.acc_flux_itor]),
                    params, timer.node["simulation"])

# Stop initialization timer
timer.node["initialization"].stop()

# Define function to plot data profiles
#%matplotlib inline

x = np.arange(nb)
X = np.array(physics.engine.X, copy=False)
nc = 2

#Enthalpy plot

fig, axes = plt.subplots(2, 2, figsize=(15, 10))
# Plot pressure profile

for t in range(3):
    physics.engine.run(1e5)
    temp = compute_temperature(X, nb)
    axes[0][0].plot(x, X[0:nc*nb:nc])
    axes[1][0].plot(x, temp)
    axes[0][1].plot(x, X[1:nc*nb:nc])


plt.show()




