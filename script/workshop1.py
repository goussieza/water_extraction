# -*- coding: utf-8 -*-
"""
Created on Wed Jun  7 13:55:49 2023

@author: Antoine.Goussiez
"""

'''Import all important packages from DARTS installation'''
from darts.engines import *
from darts.models.physics_sup.property_container import property_container
from darts.models.physics_sup.physics_comp_sup import Compositional
from darts.models.physics_sup.properties_basic import Density, ViscosityConst, PhaseRelPerm, Flash, Enthalpy

import numpy as np
import matplotlib.pyplot as plt

'''Activate main timers for simulation'''
def activate_timer():
    # Call class constructor and Build timer_node object
    timer = timer_node()

    # Call object members; there are 2 types of members:
    ## <1>Function member:
    timer.start()
    ## <2>Data member:
    timer.node["simulation"] = timer_node()
    timer.node["initialization"] = timer_node()

    # Start initialization
    timer.node["initialization"].start()
    
    return timer


'''Define main parameters for simulation by overwriting default parameters'''
def define_params(max_ts=1):
    # Build a sim_params object by calling constructor
    params = sim_params()

    # Adjust time step settings
    # First time step (time unit: day)
    params.first_ts = 1e-5

    # Time step multiplier
    params.mult_ts = 4

    # Maximum time step
    params.max_ts = max_ts

    # Newton tolerance
    params.tolerance_newton = 1e-10
    
    return params



'''Mesh initialization for 1D reservoir with constant transmissibility'''
def init_mesh(nb):
    # Create mesh object by calling the class constructor
    mesh = conn_mesh()

    # Create connection list for 1D reservoir 
    block_m = np.arange(nb - 1, dtype=np.int32)
    block_p = block_m + 1

    # Set constant transmissbility
    permeability = 2
    tranD = np.ones(nb - 1) * 1e-3 * nb 
    tran = tranD * permeability

    # Initialize mesh with connection list
    mesh.init(index_vector(block_m), index_vector(block_p),
              value_vector(tran), value_vector(tranD))

    # Complete mesh initialization
    mesh.reverse_and_sort()
    
    return mesh


'''Define basic properties for the reservoir'''
def define_reservoir(nb):
    # Create numpy arrays wrapped around mesh data (no copying)
    volume = np.array(mesh.volume, copy=False)
    porosity = np.array(mesh.poro, copy=False)
    depth = np.array(mesh.depth, copy=False)
    

    # Assign volume, porosity and depth values
    volume.fill(1000 / nb)
    porosity.fill(0.2)
    depth.fill(1000)

    # Make first and last blocks large (source/sink)
    volume[0] = 1e10
    volume[nb-1] = 1e10
    
    
'''Mimic boundary conditions for the reservoir'''
def define_initial_conditions(nb):
    # Create numpy wrappers for initial solution
    pressure = np.array(mesh.pressure, copy=False) 
    fraction = np.array(mesh.composition, copy=False)
    temperature = np.array(mesh.temperature, copy=False)
    

    # Assign initial pressure values
    pressure.fill(200)
    pressure[0] = 210
    pressure[nb-1] = 190

    # Assign molar fraction values
    fraction.fill(1e-8)
    fraction[0] = 1e-8

    
    # Initial temperature conditions
    temperature.fill(350)
    temperature[0] = 350


'''Create physics from predefined properties from DARTS package'''
def define_physics():
    # basic physical parameters
    zero = 1e-15
    components_name = ['CO2', 'H2O']
    Mw = [44.01, 18.015]
    
    # activate property container
    property = property_container(phases_name=['gas', 'oil'],
                                  components_name=components_name,
                                  Mw=Mw, min_z=zero/10)

    # properties correlations
    property.flash_ev = Flash(components_name, [4.0, 0.2], zero)
    property.density_ev = dict([('gas', Density(compr=1e-3, dens0=200)),
                                ('oil', Density(compr=1e-5, dens0=600))])
    property.viscosity_ev = dict([('gas', ViscosityConst(0.05)),
                                  ('oil', ViscosityConst(0.5))])
    property.rel_perm_ev = dict([('gas', PhaseRelPerm("gas")),
                                 ('oil', PhaseRelPerm("oil"))])
    property.enthalpy_ev = dict([('gas', Enthalpy(hcap=0.035)),
                                 ('oil', Enthalpy(hcap=4.18))])

    property.rock_energy_ev=Enthalpy(hcap=1.0)
    

    # Activate physics
    physics = Compositional(property, timer, n_points=200, min_p=100, max_p=300,
                            min_z=zero/10, max_z=1-zero/10, thermal=1, min_t=250, max_t=370)
    
            
    

    return physics


# create all model parameters
nb = 50
timer = activate_timer()
params = define_params(max_ts=1)
mesh = init_mesh(nb)
define_reservoir(nb)
define_initial_conditions(nb)
physics = define_physics()
#physics.set_uniform_T_initial_conditions(mesh,200,[4.0, 0.2],350)


# Initialize engine
physics.engine.init(mesh, ms_well_vector(),
                    op_vector([physics.acc_flux_itor[0]]),
                    params, timer.node["simulation"])

# Stop initialization timer
timer.node["initialization"].stop()

# Run simulator for 500 days
physics.engine.run(500)

# Print timers (note where most of the time was spent!)
print(timer.print("", ""))



# Define function to plot data profiles
#%matplotlib inline
def plot_profile(data, name, sp, ax):
    n = len(data)    
    ax.plot(np.arange(n), data[0:n], '-')
    ax.set_xlabel('Grid index')
    ax.set_ylabel('%s' % (name))  
    
    
    
# Get numpy wrapper for final solution
X = np.array(physics.engine.X, copy=False)

# Prepare for plotting
nc = 3

fig, axes = plt.subplots(1, 3, figsize=(10, 4))
# Plot pressure profile
plot_profile(X[0:nc*nb:nc],'Pressure, bar', 1, axes[0])
# Plot molar fraction profile
plot_profile(X[1:nc*nb:nc],'Zc, -', 2, axes[1])

plot_profile(X[2:nc*nb:nc],'Temperature, K', 2, axes[2])

plt.show()
























































