# -*- coding: utf-8 -*-
"""
Created on Wed Jun  7 09:28:56 2023

@author: Antoine.Goussiez
"""


#STEP1

'''Import all important packages from DARTS installation'''
from darts.engines import *

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


from modified_classes.geothermal_custom import Geothermal_custom
from modified_classes.custom_evaluators import *
from Diagram_Ph import Diagram_Ph
from modified_classes.regression_functions import h_steam

from iapws import IAPWS95

redirect_darts_output('run_water_lowpressure.log')

#STEP2

'''Activate main timers for simulation'''
def activate_timer():
    # Call class constructor and Build timer_node object
    timer = timer_node()

    # Call object members; there are 2 types of members:
    ## <1>Function member:
    timer.start()
    ## <2>Data member:
    timer.node["simulation"] = timer_node()
    timer.node["initialization"] = timer_node()

    # Start initialization
    timer.node["initialization"].start()
    
    return timer

#STEP3

'''Define main parameters for simulation by overwriting default parameters'''
def define_params(max_ts=365):
    # Build a sim_params object by calling constructor
    params = sim_params()

    # Adjust time step settings
    # First time step (time unit: day)
    params.first_ts = 0.01

    # Time step multiplier
    params.mult_ts = 2

    # Maximum time step
    params.max_ts = max_ts

    # Newton tolerance
    params.tolerance_newton = 1e-8
    params.tolerance_linear = 1e-8
    
    return params


#STEP4

'''Mesh initialization for 1D reservoir with constant transmissibility'''
def init_mesh(nb):
    # Create mesh object by calling the class constructor
    mesh = conn_mesh()

    # Create connection list for 1D reservoir 
    block_m = np.arange(nb - 1, dtype=np.int32)
    block_p = block_m + 1

    # Set constant transmissbility
    permeability = 2
    tranD = np.ones(nb - 1) * 1e-3 * nb 
    tran = tranD * permeability
    # Initialize mesh with connection list
    mesh.init(index_vector(block_m), index_vector(block_p),
              value_vector(tran), value_vector(tranD))

    # Complete mesh initialization
    mesh.reverse_and_sort()
    
    return mesh

#STEP5

#Building a 1*1*1 m3 reservoir

'''Define basic properties for the reservoir'''
def define_reservoir(nb):
    # Create numpy arrays wrapped around mesh data (no copying)
    volume = np.array(mesh.volume, copy=False)
    porosity = np.array(mesh.poro, copy=False)
    depth = np.array(mesh.depth, copy=False)

    # Assign volume, porosity and depth values
    
    #Building a 1*1*1 m3 reservoir

    volume.fill(1/nb)
    porosity.fill(0.5)
    depth.fill(1)

    # Make first and last blocks large (source/sink)
    volume[0] = 1e10
    volume[nb-1] = 1e10
#STEP6


'''Mimic boundary conditions for the reservoir'''
def define_initial_conditions(nb,T_atm,p_atm,p0,T0):
    # Create numpy wrappers for initial solution
    
    state = value_vector([p_atm, 0])
    E = custom_total_enthalpy_evalutor(T_atm)
    enth = E.evaluate(state)
    E0 = custom_total_enthalpy_evalutor(T0)
    enth0= E0.evaluate(state)

    
    
    pressure = np.array(mesh.pressure, copy=False) 
    enthalpy = np.array(mesh.enthalpy, copy=False)
    

    # Assign initial pressure values
    pressure.fill(p_atm)
    pressure[0] = p0
    
    enthalpy.fill(enth)
    # enthalpy.fill(h_atm)
    enthalpy[0]=enth0

#STEP7

'''Create physics from predefined properties from DARTS package'''
def define_physics():
    # basic physical parameters

    # Activate physics
    physics = Geothermal_custom(timer, n_points=1000, min_p=1e-18, max_p=6e-3, min_e=-600*18.015, max_e=2800*18.015)
    

    return physics

#STEP8

def compute_temperature(X,nb):
    from modified_classes.Water_properties import Water_properties
    p_list=X[0:2 * nb:2] 
    h_list=X[1:2 * nb:2]
    T_list=[Water_properties([p,h]).T for p, h in zip(p_list, h_list)]
    return T_list

def compute_saturation(X,nb):
    from modified_classes.Water_properties import Water_properties
    p_list=X[0:2 * nb:2] 
    h_list=X[1:2 * nb:2]
    ss_list=[Water_properties([p,h]).steam_saturation for p, h in zip(p_list, h_list)]
    return ss_list
    
def molar_h_to_spec(X,nb):
    return X[1:2 * nb:2]/18.015

def states_list(X,nb):
    p_list=X[0:2 * nb:2] 
    h_list=X[1:2 * nb:2]
    states_list=list(zip(p_list, h_list))
    return states_list

def temp_to_enth_steam(temp):
    h=[h_steam(t) for t in temp]
    return h

def compute_steam_density(X, nb):
    from modified_classes.Water_properties import Water_properties
    p_list = X[0:2 * nb:2] 
    h_list = X[1:2 * nb:2]
    ss_list = compute_saturation(X, nb)

    density_list = []

    for p, h, ss in zip(p_list, h_list, ss_list):
        if ss == 1:
            density_list.append(Water_properties([p, h]).overheated_steam_density)  # Replace method1 with actual method name
        elif ss == 0:
            density_list.append(0)
        else:
            density_list.append(Water_properties([p, h]).saturated_steam_density)  # Replace method2 with actual method name

    return density_list

def plot_profile(data, name, ax):
    n = len(data)    
    ax.plot(np.arange(n), data[0:n], '-')
    ax.set_xlabel('Grid index')
    ax.set_ylabel('%s' % (name))  # Get numpy wrapper for final solution    



        
'''RUN1 : Single phase steam'''
nb = 30
timer = activate_timer()
params = define_params(max_ts=365)
mesh = init_mesh(nb)
define_reservoir(nb)
define_initial_conditions(nb, T_atm=250, p_atm=3e-10, T0=270, p0=3e-5)
physics = define_physics()

# Initialize engine
physics.engine.init(mesh, ms_well_vector(),
                    op_vector([physics.acc_flux_itor]),
                    params, timer.node["simulation"])

# Stop initialization timer
timer.node["initialization"].stop()

# Run simulator for 500 days
physics.engine.run(365)

# Print timers (note where most of the time was spent!)
print(timer.print("", ""))


'''Data processing'''

X = np.array(physics.engine.X, copy=False)
states_run1=states_list(X, nb)

nc = 2

#Enthalpy plot

fig, axes = plt.subplots(2, 3, figsize=(15, 10))
# Plot pressure profile
plot_profile(X[0:nc*nb:nc],'Pressure, bar', axes[0,0])

# Plot molar enthalpy profile
plot_profile(molar_h_to_spec(X,nb),'Specific Enthalpy, kJ/kg', axes[1,0])

# Plot temperature profile
plot_profile(compute_temperature(X, nb),'Temperature, K', axes[0,1])

# Plot steam saturation
plot_profile(compute_saturation(X, nb),'Steam Saturation', axes[1,1])

# Plot steam density 
plot_profile(compute_steam_density(X, nb), 'Steam density, kg/m3', axes[1,2])

plt.suptitle('RUN1 : Single phase steam \n Condition: T_atm=250K and P_atm=3e-10 bar \n Injected steam: T_steam=270K, P_steam=3e-5 bar')
plt.tight_layout()
plt.show()


# '''RUN2 : Single phase ICE'''
# nb = 30
# timer = activate_timer()
# params = define_params(max_ts=365)
# mesh = init_mesh(nb)
# define_reservoir(nb)
# define_initial_conditions(nb, T_atm=100, p_atm=3e-10, T0=100, p0=3e-10)
# physics = define_physics()

# # Initialize engine
# physics.engine.init(mesh, ms_well_vector(),
#                     op_vector([physics.acc_flux_itor]),
#                     params, timer.node["simulation"])

# # Stop initialization timer
# timer.node["initialization"].stop()

# # Run simulator for 500 days
# physics.engine.run(365*20)

# # Print timers (note where most of the time was spent!)
# print(timer.print("", ""))


# '''Data processing'''

# X = np.array(physics.engine.X, copy=False)
# states_run2=states_list(X, nb)

# nc = 2

# #Enthalpy plot

# fig, axes = plt.subplots(2, 3, figsize=(15, 10))
# # Plot pressure profile
# plot_profile(X[0:nc*nb:nc],'Pressure, bar', axes[0,0])

# # Plot molar enthalpy profile
# plot_profile(molar_h_to_spec(X,nb),'Specific Enthalpy, kJ/kg', axes[1,0])

# # Plot temperature profile
# plot_profile(compute_temperature(X, nb),'Temperature, K', axes[0,1])

# # Plot steam saturation
# plot_profile(compute_saturation(X, nb),'Steam Saturation', axes[1,1])

# # Plot steam density 
# plot_profile(compute_steam_density(X, nb), 'Steam density, kg/m3', axes[1,2])
# plt.suptitle('RUN2 : Single phase ICE \n Condition: T_atm=50K and P_atm=3e-10 bar')
# plt.tight_layout()
# plt.show()



# '''RUN3 : Realistic condition with steam injected'''
# nb = 30
# timer = activate_timer()
# params = define_params(max_ts=365)
# mesh = init_mesh(nb)
# define_reservoir(nb)
# define_initial_conditions(nb, T_atm=50, p_atm=3e-10, T0=270, p0=6e-10)
# physics = define_physics()

# # Initialize engine
# physics.engine.init(mesh, ms_well_vector(),
#                     op_vector([physics.acc_flux_itor]),
#                     params, timer.node["simulation"])

# # Stop initialization timer
# timer.node["initialization"].stop()

# # Run simulator for 500 days
# physics.engine.run(365*2000)

# # Print timers (note where most of the time was spent!)
# print(timer.print("", ""))


# '''Data processing'''

# X = np.array(physics.engine.X, copy=False)
# states_run3=states_list(X, nb)

# nc = 2

# #Enthalpy plot

# fig, axes = plt.subplots(2, 3, figsize=(15, 10))
# # Plot pressure profile
# plot_profile(X[0:nc*nb:nc],'Pressure, bar', axes[0,0])

# # Plot molar enthalpy profile
# plot_profile(molar_h_to_spec(X,nb),'Specific Enthalpy, kJ/kg', axes[1,0])

# # Plot temperature profile
# plot_profile(compute_temperature(X, nb),'Temperature, K', axes[0,1])

# # Plot steam saturation
# plot_profile(compute_saturation(X, nb),'Steam Saturation', axes[1,1])

# # Plot steam density 
# plot_profile(compute_steam_density(X, nb), 'Steam density, kg/m3', axes[1,2])
# plt.suptitle('RUN3 : Moon conditions \n Condition: T_atm=50K and P_atm=3e-10 bar \n Injected steam: T0=250K, P0=6e-10 bar')
# plt.tight_layout()
# plt.show()


# '''RUN4 : Non Realistic condition with steam injected crossing the saturation curve but still hight steam sat'''

# nb = 30
# timer = activate_timer()
# params = define_params(max_ts=365)
# mesh = init_mesh(nb)
# define_reservoir(nb)
# define_initial_conditions(nb, T_atm=200, p_atm=3e-10, T0=250, p0=6e-5)
# physics = define_physics()

# # Initialize engine
# physics.engine.init(mesh, ms_well_vector(),
#                     op_vector([physics.acc_flux_itor]),
#                     params, timer.node["simulation"])

# # Stop initialization timer
# timer.node["initialization"].stop()

# # Run simulator for 500 days
# physics.engine.run(365*20)

# # Print timers (note where most of the time was spent!)
# print(timer.print("", ""))


# '''Data processing'''

# X = np.array(physics.engine.X, copy=False)
# states_run4=states_list(X, nb)

# nc = 2

# #Enthalpy plot

# fig, axes = plt.subplots(2, 3, figsize=(15, 10))
# # Plot pressure profile
# plot_profile(X[0:nc*nb:nc],'Pressure, bar', axes[0,0])

# # Plot molar enthalpy profile
# plot_profile(molar_h_to_spec(X,nb),'Specific Enthalpy, kJ/kg', axes[1,0])

# # Plot temperature profile
# plot_profile(compute_temperature(X, nb),'Temperature, K', axes[0,1])

# # Plot steam saturation
# plot_profile(compute_saturation(X, nb),'Steam Saturation', axes[1,1])

# # Plot steam density 
# plot_profile(compute_steam_density(X, nb), 'Steam density, kg/m3', axes[1,2])
# plt.suptitle('RUN4 :  Non Realistic condition with steam injected crossing the saturation curve but still steam sat close to 1 \n Condition: T_atm=200K and P_atm=3e-10 bar \n Injected steam: T0=250K, P0=6e-10 bar')
# plt.tight_layout()
# plt.show()


# diagram = Diagram_Ph(1e-15, 1e-3, -15000, 49000)
# diagram.activate_h_sat()
# diagram.activate_h_sub()
# diagram.plot_states(states_run1, 'Single phase steam','green')
# diagram.plot_states(states_run2, 'Single phase ice','red')
# diagram.plot_states(states_run3, 'Moon condition','blue')
# diagram.plot_states(states_run4, 'Steam crossing','violet')
# diagram.plot_map()



# pressure = [row[0] for row in data_control]
# h_system = [row[1] for row in data_control]
# T = [row[2] for row in data_control]
# steam_saturation = [row[3] for row in data_control]
# rho = [row[4] for row in data_control]
# steam_enthalpy = [row[5] for row in data_control]




# # Graphe pour pressure
# plot_profile(pressure, 'Pressure', axes[0,0])

# # Graphe pour h_system
# plot_profile(h_system, 'H_system', axes[1,0])

# # Graphe pour T
# plot_profile(T, 'Temperature (T)', axes[0,1])

# # Graphe pour steam_saturation
# plot_profile(steam_saturation, 'Steam Saturation', axes[1,1])

# # Graphe pour rho
# plot_profile(rho, 'Density (rho)', axes[1,2])

# # Graphe pour steam_enthalpy
# plot_profile(steam_enthalpy, 'Steam Enthalpy', axes[0,2])

# plt.tight_layout()  # Ajustement automatique des espacements entre les sous-graphiques
# plt.show()








