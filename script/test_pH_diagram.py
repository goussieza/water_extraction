
import numpy as np
import matplotlib.pyplot as plt

from modified_classes.Water_properties import Water_properties
from iapws import _Ice
from modified_classes.Ideal_gas import Ideal_gas
from modified_classes.regression_functions import Sublimation_temperature



class Diagram_Ph:
    
    def __init__(self, P_min, P_max, h_min, h_max, n=100):
        self.P_range = np.logspace(np.log10(P_min), np.log10(P_max), n) #bar
        self.h_range = np.linspace(h_min, h_max, n) #molar enthalpy
        self.matrix = self._create_matrix()
        self.isotherms = self._create_isotherms([120, 140, 160, 180,200,220,260]) # add the desired isotherms temperatures
        # self.isodensity = self._create_isodensity([10,100,300,500,700,1000]) # add the desired density values

    def _create_matrix(self):
        matrix = np.empty((len(self.P_range), len(self.h_range)), dtype=object)

        for i, h in enumerate(self.h_range):
            for j, P in enumerate(self.P_range):
                state=[P,h]
                matrix[i, j] = Water_properties(state)

        return matrix
    
    def _create_isotherms(self, T_values):
        isotherms = {}
        for T in T_values:
            h_values = []
            for P in self.P_range:
                h_low = min(self.h_range)
                h_high = max(self.h_range)
                for _ in range(100): # limit the number of iterations
                    h_mid = (h_low + h_high) / 2
                    water = Water_properties([P, h_mid])
                    if water.T > T:
                        h_high = h_mid
                    else:
                        h_low = h_mid
                h_values.append(h_mid)
            isotherms[T] = h_values
        return isotherms
    
    
    def _create_isodensity(self, rho_values):
       isodensity = {}
       for rho in rho_values:
           h_values = []
           for P in self.P_range:
               h_low = min(self.h_range)
               h_high = max(self.h_range)
               for _ in range(100): # limit the number of iterations
                   h_mid = (h_low + h_high) / 2
                   water = Water_properties([P, h_mid])
                   if water.TP_density > rho:
                       h_high = h_mid
                   else:
                       h_low = h_mid
               h_values.append(h_mid)
           isodensity[rho] = h_values
       return isodensity
    
    def plot_map(self, property_name):
        
        plt.figure(figsize=(10, 7))

        
        property_map = np.empty((len(self.P_range), len(self.h_range)))

        for i, h in enumerate(self.h_range):
            for j, P in enumerate(self.P_range):
                if property_name == "mu" and self.matrix[i, j].phase == "twophase":
                    property_value = None
                else:
                    property_value = getattr(self.matrix[i, j], property_name)
                property_map[i, j] = property_value

        fig, ax = plt.subplots()
        X, Y = np.meshgrid(self.h_range, self.P_range)
        img = ax.pcolormesh(X, Y, property_map.T, cmap='viridis', shading='auto')
        cbar = fig.colorbar(img, ax=ax, label=property_name)      
        
        # Isotherms
        for T, h_values in self.isotherms.items():
            ax.plot(h_values, self.P_range, '--',linewidth=.8, label=f'{T}K isotherm')
            
        # # Isodensity
        # for rho, h_values in self.isodensity.items():
        #     ax.plot(h_values, self.P_range, '-.', linewidth=0.9, label=f'{rho}kg/m³ isodensity')

        
        #Sublimation and saturation curve
        
        
        h_sub_range=[_Ice(Sublimation_temperature(P), P)['h']*18.015 for P in self.P_range]
        h_sat_range=[Ideal_gas(T=Sublimation_temperature(P),P=P).h *18.015 for P in self.P_range]
        
        ax.plot(h_sub_range, self.P_range, color='white', linewidth=1.1, label='Sublimation curve')
        ax.plot(h_sat_range, self.P_range, color='red', linewidth=1.1, label='Saturation curve')       
        
        
        
            
        
        ax.set_xlabel('Enthalpy (kJ/kmol)')
        ax.set_ylabel('Pressure (bar)')
        ax.set_title(f'{property_name} Map')
        ax.set_yscale('log')
        
        ax.legend(bbox_to_anchor=(1.25, 1), loc='upper left')
        plt.show()

Diagram_Ph(1e-15, 1e-3, -15000, 49000).plot_map('TP_density')
# Diagram_Ph(1e-14, 1e-3, -15000, 49000).plot_map('T')

