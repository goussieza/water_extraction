# -*- coding: utf-8 -*-
"""
Created on Wed Jun  7 09:28:56 2023

@author: Antoine.Goussiez
"""


#STEP1

'''Import all important packages from DARTS installation'''
from darts.engines import *
from darts.models.physics_sup.property_container import property_container
from darts.models.physics_sup.physics_comp_sup import Compositional
from darts.models.physics_sup.properties_basic import Density, ViscosityConst, PhaseRelPerm, Flash
from darts.models.physics.geothermal import Geothermal

import numpy as np
import matplotlib.pyplot as plt


import sys
sys.path.insert(0, 'C:\\Users\\antoine.goussiez\\.spyder-py3\\simulations\\modified_classes')
from geothermal_custom_v2 import Geothermal_custom_v2
from custom_evaluators import *

redirect_darts_output('run_geothermal.log')


#STEP2

'''Activate main timers for simulation'''
def activate_timer():
    # Call class constructor and Build timer_node object
    timer = timer_node()

    # Call object members; there are 2 types of members:
    ## <1>Function member:
    timer.start()
    ## <2>Data member:
    timer.node["simulation"] = timer_node()
    timer.node["initialization"] = timer_node()

    # Start initialization
    timer.node["initialization"].start()
    
    return timer



#STEP3

'''Define main parameters for simulation by overwriting default parameters'''
def define_params(max_ts=365):
    # Build a sim_params object by calling constructor
    params = sim_params()

    # Adjust time step settings
    # First time step (time unit: day)
    params.first_ts = 0.01

    # Time step multiplier
    params.mult_ts = 10

    # Maximum time step
    params.max_ts = max_ts

    # Newton tolerance
    params.tolerance_newton = 1e-1
    
    return params


#STEP4

'''Mesh initialization for 1D reservoir with constant transmissibility'''
def init_mesh(nb):
    # Create mesh object by calling the class constructor
    mesh = conn_mesh()

    # Create connection list for 1D reservoir 
    block_m = np.arange(nb - 1, dtype=np.int32)
    block_p = block_m + 1

    # Set constant transmissbility
    permeability = 2
    tranD = np.ones(nb - 1) * 1e-3 * nb 
    tran = tranD * permeability

    # Initialize mesh with connection list
    mesh.init(index_vector(block_m), index_vector(block_p),
              value_vector(tran), value_vector(tranD))

    # Complete mesh initialization
    mesh.reverse_and_sort()
    
    return mesh

#STEP5

'''Define basic properties for the reservoir'''
def define_reservoir(nb):
    # Create numpy arrays wrapped around mesh data (no copying)
    volume = np.array(mesh.volume, copy=False)
    porosity = np.array(mesh.poro, copy=False)
    depth = np.array(mesh.depth, copy=False)

    # Assign volume, porosity and depth values
    volume.fill(3000 / nb)
    porosity.fill(0.5)
    depth.fill(1000)

    # Make first and last blocks large (source/sink)
    volume[0] = 1e10
    volume[nb-1] = 1e10
    

#STEP6

'''Mimic boundary conditions for the reservoir'''
def define_initial_conditions(nb):
    # Create numpy wrappers for initial solution
    pressure = np.array(mesh.pressure, copy=False) 
    fraction = np.array(mesh.composition, copy=False)

    # Assign initial pressure values
    pressure.fill(3e-10)
    

    # Assign molar fraction values
    fraction.fill(0.1)
    
    
#STEP7

'''Create physics from predefined properties from DARTS package'''
def define_physics():
    # basic physical parameters
    zero = 1e-8
    components_name = ['H2O']
    Mw = [18.015]
    
    # activate property container
    property = property_container(phases_name=['steam'],
                                  components_name=components_name,
                                  Mw=Mw, min_z=zero/10)

    # properties correlations
    property.flash_ev = Flash(components_name,[0.2], zero)

    property.density_ev = dict([('steam', custom_steam_density_evaluator())])
    property.viscosity_ev = dict([('steam', custom_steam_viscosity_evaluator())])
    property.rel_perm_ev = dict([('steam', custom_steam_relperm_evaluator())])
    

    # Activate physics
    physics = Compositional(property, timer, n_points=nb, min_p=3e-10, max_p=6e-3, thermal=1, min_t=125, max_t=273, min_z=zero/10, max_z=1-zero/10)
    

    return physics


#STEP8

'''create all model parameters'''
nb = 50
timer = activate_timer()
params = define_params(max_ts=10)
mesh = init_mesh(nb)
define_reservoir(nb)
define_initial_conditions(nb)
physics = define_physics() #compositional object

# Initialize engine
physics.engine.init(mesh, ms_well_vector(),
                    op_vector([physics.acc_flux_itor[0]]),
                    params, timer.node["simulation"])

# Stop initialization timer
timer.node["initialization"].stop()

# Run simulator for 500 days
physics.engine.run(5000)

# Print timers (note where most of the time was spent!)
print(timer.print("", ""))

#Data processing

# Define function to plot data profiles
#%matplotlib inline
def plot_profile(data, name, sp, ax):
    n = len(data)    
    ax.plot(np.arange(n), data[0:n], '-')
    ax.set_xlabel('Grid index')
    ax.set_ylabel('%s' % (name))  # Get numpy wrapper for final solution
X = np.array(physics.engine.X, copy=False)

# Prepare for plotting
fig = plt.figure()   

nc = 2

fig, axes = plt.subplots(1, 2, figsize=(10, 4))
# Plot pressure profile
plot_profile(X[0:nc*nb:nc],'Pressure, bar', 1, axes[0])
# Plot molar fraction profile
plot_profile(X[1:nc*nb:nc],'Molar fraction', 2, axes[1])